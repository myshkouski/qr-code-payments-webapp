export default defineNuxtConfig({
  builder: "webpack",
  css: [
    'vuetify/styles',
    '@mdi/font/css/materialdesignicons.css'
  ],
  srcDir: "src",
  build: {
    transpile: ['vuetify'],
  },
  modules: [
    '@vueuse/nuxt'
  ],
  experimental: {
    payloadExtraction: false
  }
})
