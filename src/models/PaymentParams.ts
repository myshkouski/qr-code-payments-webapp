export interface PaymentParams {
  currency: string
  serviceNo: string
  countryCode?: string
  amount?: number
  order?: string
  paymentProvider: string
}
