export const componentRef = <T extends abstract new (...args: any) => any>() => ref<InstanceType<T> | null>(null)
